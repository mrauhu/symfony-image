<?php

namespace MrauHu\ImageBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MrauHu\ImageBundle\Entity\Image;
use MrauHu\ImageBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    public function indexAction() {

		$image = new Image();
		$user = $this->get( 'security.context' )->getToken()->getUser();

		$form = $this->createFormBuilder( $image )
				->add( 'file', null, array(
					'label' => 'Выберите файл',
					'attr' => array( 'accept' => 'image/jpeg,image/pjpeg,image/png,image/gif')) )
				->getForm();

		if ( $this->getRequest()->getMethod() === 'POST' ) {
			$form->bind	( $this->getRequest() );
			if ( $form->isValid() ) {
				
					$image->setUser( $user );

					$em = $this->getDoctrine()->getManager();
					$em->persist($image);
					$em->flush();
					
				if ( $this->getRequest()->isXmlHttpRequest() ) {
					$response = new JsonResponse();
					
					return $response->setData( 'success' );
					
				}
			} else {
				if ( $this->getRequest()->isXmlHttpRequest() ) {

					$response = new JsonResponse();
					return $response->setData('error');
				}
			}
		}
		
		return $this->render( 'MrauHuImageBundle:Default:index.html.twig', array(
			'form' => $form->createView(),
			'user' => $user
		));
	}
	
	function deleteAction ( $id ) {
		
		$em = $this->getDoctrine()->getManager();
		
		$image = $em->getRepository('MrauHuImageBundle:Image')->find( $id );
		if ( !$image ) {
			 throw $this->createNotFoundException('Не найдено изображение №' . $id );
		}

		$user = $this->get( 'security.context' )->getToken()->getUser();
		if ( $image->getUser()->getId() !== $user->getId() ) {
			throw $this->createNotFoundException('Изображение не принадлежит пользователю' );
		}

		$em->remove( $image );
		$em->flush();

		return $this->redirect( $this->generateUrl( 'mrauhu_image_homepage' ) );
	}


}
