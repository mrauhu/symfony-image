// Initialize Zurb Foundation
$(document).foundation();

Dropzone.autoDiscover = false;

$(document).ready(function() {
	// Загрузка файлов с формы #upload
	var myDropzone = new Dropzone( "#upload", { 
		parallelUploads: 5,
		paramName: 'form[file]',
		acceptedMimeTypes : 'image/jpeg,image/pjpeg,image/png,image/gif',
		maxFilesize: 1,
		
		dictDefaultMessage: "Перетащите сюда файлы для загрузки",
		dictFallbackMessage: "Ваш браузер не поддерживает перетаскивание",
		dictFallbackText: "Используйте стандартную форму загрузки",
		dictInvalidFileType: "Неправильный тип файла, загружайте изображения",
		dictFileTooBig: "Превышен максимальный размер файла",
		init: function () {
			$('input#form_file').hide();
		},
	});

	// Получение информации об ошибки
	myDropzone.on('success', function( response ){
		if ( response.match('error') ) {
			console.log('Ошибка загрузки')
		}
	})
});


