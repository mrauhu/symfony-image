<?php

namespace MrauHu\ImageBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="user", cascade={"persist"} )
	 * 
     **/
	protected $images;

    public function __construct()
    {
        parent::__construct();
		
		$this->images = new ArrayCollection();
    }
	
	
	public function getImages() {
		return $this->images->toArray();
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add images
     *
     * @param \MrauHu\ImageBundle\Entity\Image $images
     * @return User
     */
    public function addImage(\MrauHu\ImageBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \MrauHu\ImageBundle\Entity\Image $images
     */
    public function removeImage(\MrauHu\ImageBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }
}
