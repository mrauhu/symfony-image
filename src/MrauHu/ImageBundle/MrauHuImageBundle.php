<?php

namespace MrauHu\ImageBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MrauHuImageBundle extends Bundle
{
	public function getParent() {
		return 'FOSUserBundle';
	}
}
